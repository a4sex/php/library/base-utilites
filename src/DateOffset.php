<?php

namespace A4Sex\BaseUtilites;

class DateOffset
{
    const DSTR = 'Y-m-d H:i:s';

    public static function offset(int $sec, ?int $time = null, bool $negative = false): string
    {
        if ($negative) {
            $sec = $sec * -1;
        }
        if (!$time) {
            $time = time();
        }
        $date = new \DateTime();
        $date->setTimestamp($time);

        return $date->modify("$sec sec")->format(self::DSTR);
    }

    public static function DateTime($mySQLDate): \DateTime
    {
        return \DateTime::createFromFormat(self::DSTR, $mySQLDate);
    }

    public static function decSec(?int $sec = 1, ?int $time = null): string
    {
        return self::offset($sec, $time, true);
    }

    public static function decMin(?int $min = 1, ?int $time = null): string
    {
        return self::decSec(60 * $min, $time);
    }

    public static function decHour(?int $hour = 1, ?int $time = null): string
    {
        return self::decSec(60 * 60 * $hour, $time);
    }

    public static function decDay(?int $day = 1, ?int $time = null): string
    {
        return self::decSec(60 * 60 * 24 * $day, $time);
    }

    // Расхождение по суткам
    public static function decMonth(?int $month = 1, ?int $time = null): string
    {
        return self::decSec(60 * 60 * 24 * 30 * $month, $time);
    }


    public static function incSec(?int $sec = 1, ?int $time = null): string
    {
        return self::offset($sec, $time, false);
    }

    public static function incMin(?int $min = 1, ?int $time = null): string
    {
        return self::incSec(60 * $min, $time);
    }

    public static function incHour(?int $hour = 1, ?int $time = null): string
    {
        return self::incSec(60 * 60 * $hour, $time);
    }

    public static function incDay(?int $day = 1, ?int $time = null): string
    {
        return self::incSec(60 * 60 * 24 * $day, $time);
    }

    // Расхождение по суткам
    public static function incMonth(?int $month = 1, ?int $time = null): string
    {
        return self::incSec(60 * 60 * 24 * 30 * $month, $time);
    }
}
