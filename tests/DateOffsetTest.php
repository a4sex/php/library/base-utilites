<?php


use A4Sex\BaseUtilites\DateOffset;
use PHPUnit\Framework\TestCase;

class DateOffsetTest extends TestCase
{

    public function setUp(): void
    {
        date_default_timezone_set('UTC');
    }

    public function testDecMin()
    {
        self::assertEquals('2023-07-07 11:30:19', DateOffset::decMin(min: 2, time: 1688729539));
    }

    public function testIncHour()
    {
        self::assertEquals('2023-07-07 13:32:19', DateOffset::incHour(hour: 2, time: 1688729539));
    }

    public function testIncMin()
    {
        self::assertEquals('2023-07-07 11:34:19', DateOffset::incMin(min: 2, time: 1688729539));
    }

    public function testDateTime()
    {
        self::assertInstanceOf(\DateTime::class, DateOffset::DateTime('2023-07-07 11:34:19'));
    }

    public function testDecHour()
    {
        self::assertEquals('2023-07-07 09:32:19', DateOffset::decHour(hour: 2, time: 1688729539));
    }

    public function testDecDay()
    {
        self::assertEquals('2023-07-05 11:32:19', DateOffset::decDay(day: 2, time: 1688729539));
    }

    public function testOffset()
    {
        self::assertEquals('2023-07-07 11:33:19', DateOffset::offset(sec: 60, time: 1688729539));
        self::assertEquals('2023-07-07 11:31:19', DateOffset::offset(sec: 60, time: 1688729539, negative: true));
    }

    public function testIncDay()
    {
        self::assertEquals('2023-07-09 11:32:19', DateOffset::incDay(day: 2, time: 1688729539));
    }

    public function testDecMonth()
    {
        self::assertEquals('2023-05-08 11:32:19', DateOffset::decMonth(month: 2, time: 1688729539));
    }

    public function testIncSec()
    {
        self::assertEquals('2023-07-07 11:32:21', DateOffset::incSec(sec: 2, time: 1688729539));
    }

    public function testIncMonth()
    {
        self::assertEquals('2023-09-05 11:32:19', DateOffset::incMonth(month: 2, time: 1688729539));
    }

    public function testDecSec()
    {
        self::assertEquals('2023-07-07 11:32:17', DateOffset::decSec(sec: 2, time: 1688729539));
    }
}
